const axios = require("axios");

const modificarTarea = async () => {
  // Modifica la tarea en backend
  const respuesta = await axios.patch(
    "http://localhost:3000/tareas/6196c6ab1623ff76de4951b0",
    {
      titulo: "Ver televisión",
      terminada: true,
    }
  );
  console.log(respuesta.data);
};

modificarTarea();
