const axios = require("axios");

const crearTarea = async () => {
  // Crea la tarea en backend
  const respuesta = await axios.post("http://localhost:3000/tareas", {
    titulo: "Watch TV",
  });
  console.log(respuesta.data);
};

crearTarea();
