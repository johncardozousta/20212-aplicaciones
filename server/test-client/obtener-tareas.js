const axios = require("axios");

const obtenerTareas = async () => {
  // Obtiene las tareas del backend
  const tareas = await axios.get("http://localhost:3000/tareas");
  console.log(tareas.data);
};

obtenerTareas();
