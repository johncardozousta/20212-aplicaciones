// Importa paquetes
const express = require("express");
const Tarea = require("../models/tarea");

// Crea el router
const router = express.Router();

// -- RUTAS DEL API --
// Obtiene todas las tareas
router.get("/", async (req, res) => {
  try {
    // Obtiene la lista de tareas de la BD
    const tareas = await Tarea.find();

    // Retorna las tareas al cliente
    return res.status(200).send(tareas);
  } catch (error) {
    res.status(500).send({ mensaje: "Hubo un error en el servidor" });
  }
});

// Obtiene una tarea
router.get("/:id", async (req, res) => {
  try {
    // Obtiene el id del parámetro
    const id = req.params.id;

    // Obtiene la tarea con el id recibido
    const tarea = await Tarea.findById(id);

    // Retorna la tarea el cliente
    if (tarea === null) {
      return res.status(404).send({ mensaje: "No se encontró la tarea" });
    } else {
      return res.status(200).send(tarea);
    }
  } catch (error) {
    res.status(500).send({ mensaje: "Hubo un error en el servidor" });
  }
});

// Crea una nueva tarea
router.post("/", async (req, res) => {
  // Obtiene el body
  const { titulo, descripcion, terminada, fecha } = req.body;

  try {
    // Crea el objeto Tarea
    const tarea = new Tarea({
      titulo,
      descripcion,
      terminada,
      fecha,
    });
    // Guarda el objeto en la base de datos
    let resultado = await tarea.save();
    // Retorna el objeto creado al cliente
    res.status(201).send(resultado);
  } catch (error) {
    res.status(500).send({ mensaje: "Hubo un error en el servidor" });
  }
});

// Modifica una tarea
router.patch("/:id", async (req, res) => {
  // Obtiene el id de la tarea
  const id = req.params.id;
  // Obtiene el body de la petición
  const body = req.body;

  try {
    // Actualiza la tarea dado su id
    const resultado = await Tarea.findOneAndUpdate({ _id: id }, body, {
      new: true,
    });
    // retorna el objeto actualizado al cliente
    return res.status(200).send(resultado);
  } catch (error) {
    res.status(500).send({ mensaje: "Hubo un error en el servidor" });
  }
});

// Elimina una tarea
router.delete("/:id", async (req, res) => {
  // Obtiene el id de los parámetros
  const { id } = req.params;

  try {
    // Elimina la tarea de la BD
    const resultado = await Tarea.deleteOne({
      _id: id,
    });

    // Retorna la respuesta al cliente
    return res.status(200).send(resultado);
  } catch (error) {
    res.status(500).send({ mensaje: "Hubo un error en el servidor" });
  }
});

module.exports = router;
