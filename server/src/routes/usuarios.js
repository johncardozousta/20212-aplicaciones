// Importa ExpressJS
const express = require("express");

// Crea el router
const router = express.Router();

router.get("/", (req, res) => {
  res
    .status(200)
    .send([{ nombre: "hugo" }, { nombre: "paco" }, { nombre: "luis" }]);
});

module.exports = router;
