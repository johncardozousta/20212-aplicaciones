// Importa paquetes externos
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();

// Establece las rutas
const tareasRoute = require("./routes/tareas");
const usuariosRoute = require("./routes/usuarios");

// Crea la aplicación
const app = express();

// Permite leer el body de las peticiones
app.use(express.json());

// Habilita peticiones desde servidores remotos
app.use(cors());

// Middleware de rutas
app.use("/tareas", tareasRoute);
app.use("/usuarios", usuariosRoute);

// Rutas
app.get("/", (req, res) => {
  res.send("Bienvenido al API");
});

// Conexion a la base de datos
mongoose.connect(
  // Cadena de conexion
  process.env.CONEXION_MONGODB,
  // Configuración de conexión
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  },
  // Función ejecutada al conectarse a la BD
  () => {
    console.log("Conectado a la base de datos...");
  }
);

// Ejecuta el servidor
app.listen(3000);
