const mongoose = require("mongoose");

const TareaSchema = mongoose.Schema({
  titulo: {
    type: String,
    required: true,
  },
  descripcion: String,
  terminada: {
    type: Boolean,
    default: false,
  },
  fecha: {
    type: Date,
    default: Date.now,
  },
});
// Exportar esquema
module.exports = mongoose.model("Tarea", TareaSchema);
