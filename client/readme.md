# Proyecto de Aplicaciones de Softeware para Telecomunicaciones

Esta aplicación contiene el código escrito en clase.

## Curso

Aplicaciones de Software para Telecomunicaciones

## Herramientas

Las herramientas utilizadas en este proyecto son:

- Javascript
- Git
- Visual Studio Code
- Markdown
- Electron
- Nodejs
- MongoDB
- Express
- Mongoose
