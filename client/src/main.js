const { app, BrowserWindow, Menu, ipcMain } = require("electron");
const url = require("url");
const path = require("path");
const { addListener } = require("process");

let ventanaPrincipal;
let ventanaNuevaTarea;

function crearVentanaNuevaTarea() {
  // Crea la ventana para crear tarea
  ventanaNuevaTarea = new BrowserWindow({
    width: 400,
    height: 300,
    title: "Nueva tarea",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  // Elimina el menu de la ventana
  //ventanaNuevaTarea.setMenu(null);
  // Carga el archivo HTML en la ventana
  ventanaNuevaTarea.loadURL(
    url.format({
      pathname: path.join(__dirname, "views/nuevaTarea.html"),
      protocol: "file",
      slashes: true,
    })
  );

  // Escucha el evento 'closed' de la ventana de nueva tarea
  ventanaNuevaTarea.on("closed", () => {
    // Libera la memoria resetvada para la ventana
    ventanaNuevaTarea = null;
  });
}

function cargarInicio() {
  // Crea la ventana principal
  ventanaPrincipal = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  // Carga el archivo HTML en la ventana
  ventanaPrincipal.loadURL(
    url.format({
      pathname: path.join(__dirname, "views/index.html"),
      protocol: "file",
      slashes: true,
    })
  );

  // La ventana principal escucha el evento 'closed'
  ventanaPrincipal.on("closed", () => {
    // Cierra la aplicación
    app.quit();
  });

  // Si el SO es Mac ajusta el menú
  if (process.platform === "darwin") {
    templateMenu.unshift({ label: app.getName() });
  }

  // Construye el menu principal
  const menuPrincipal = Menu.buildFromTemplate(templateMenu);
  Menu.setApplicationMenu(menuPrincipal);

  // ipcMain escucha el vento 'nueva-tarea
  ipcMain.on("nueva-tarea", (evento, datos) => {
    // envia el mensaje a la ventana principal
    ventanaPrincipal.webContents.send("nueva-tarea", datos);
    // Cierra la ventana de nueva tarea
    ventanaNuevaTarea.close();
  });
}

// Ejecuta cargarInicio cuando la app está lista
app.on("ready", cargarInicio);

const templateMenu = [
  {
    label: "Tareas",
    submenu: [
      {
        label: "Nueva tarea",
        accelerator: "Ctrl+N",
        click() {
          crearVentanaNuevaTarea();
        },
      },
      {
        label: "Salir",
        accelerator: process.platform === "darwin" ? "command+Q" : "Ctrl+Q",
        click() {
          // Cierra la aplicación
          app.quit();
        },
      },
    ],
  },
];

// Verifica que la aplicación está en modo desarrollo
if (!app.isPackaged) {
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar/Ocultar DevTools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "Reload",
      },
    ],
  });
}
